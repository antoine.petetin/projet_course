package projet_course.domains.services;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import projet_course.controllers.rest.RecipeController;
import projet_course.domains.models.Unit;

@Service
public class UnitService {
    static Logger log = Logger.getLogger(RecipeController.class.getName());

    public boolean exists(String unitName){
        boolean result = false;
        try {
            result = Unit.valueOf(unitName) != null;
        }catch (Exception e){
            log.debug(e.getMessage());
        }
        return result;

    }
}


package projet_course.domains.services;

import org.springframework.stereotype.Service;
import projet_course.domains.models.*;
import projet_course.repositories.RecipeRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RecipeService {
    private RecipeRepository recipeRepository;
    private UserService userService;
    private IngredientService ingredientService;

    public RecipeService(RecipeRepository recipeRepository, UserService userService, IngredientService ingredientService) {
        this.recipeRepository = recipeRepository;
        this.userService = userService;
        this.ingredientService = ingredientService;
    }

    private Map<String, Object> recipeProjectionForFindAll(Recipe recipe){
        Map<String, Object> result = new HashMap<>();
        result.put("id", recipe.getId());
        result.put("title",recipe.getTitle());
        result.put("possible", recipe.isPossible());
        return result;
    }

    public List<Recipe> all() {
        return recipeRepository.findAll();
    }

    public List<Map<String, Object>> all(int idUser) {
        List<Map<String, Object>> results = new ArrayList<>();

        User user = userService.findById(idUser);
        if(user == null)
            return new ArrayList<>();

        for(Recipe recipe : all()){
            //findById compute if a recipe is possible or not
            results.add(recipeProjectionForFindAll(findById(recipe.getId(),idUser)));
        }

        return results;
    }

    public Recipe findById(int idRecipe, int idUser) {

        User user = userService.findById(idUser);
        if(user == null)
            return null;

        Recipe recipe = recipeRepository.findOne(idRecipe);

        //For each component in the recipe
        for(Component component : recipe.getComponents()){

            //We have to check if the recipe's component is missing
            int i=0;
            double missingQuantity = component.getQuantity();
            boolean namesMatch = false;
            while(!namesMatch && i<user.getFridge().getComponents().size()){
                Component componentInFridge = user.getFridge().getComponents().get(i);
                if(componentInFridge.getIngredient().getName().equalsIgnoreCase(component.getIngredient().getName())){
                    namesMatch = true;
                    if(componentInFridge.getQuantity() < component.getQuantity()){
                        missingQuantity -= componentInFridge.getQuantity();
                    }
                }
                i++;
            }

            //If I have the ingredient but not enough or I don't have the ingredient in my fridge,
            //the ingredient is missing !
            if((namesMatch && (missingQuantity != component.getQuantity())) || (!namesMatch)) {
                    Component missingComponent = new Component();
                    missingComponent.setIngredient(component.getIngredient());
                    missingComponent.setQuantity(missingQuantity);
                    recipe.addMissingComponent(missingComponent);
            }
        }

        return recipe;
    }

    public boolean exists(int idRecipe) {
        return recipeRepository.findOne(idRecipe) != null;
    }

    public int create(String name, String description, List<Component> components){

        int id = -1;
        Recipe recipe = new Recipe(name);
        recipe.setDescription(description);

        boolean wrongData = false;
        for(int i=0; !wrongData && i< components.size(); i++){
            //We have to verify if the ingredient exists or not, and the quantity
            if(components.get(i).getQuantity() > 0){

                Component component = new Component();
                component.setQuantity(components.get(i).getQuantity());

                Ingredient ingredient = ingredientService.findByName(components.get(i).getIngredient().getName());

                //If ingredient doesn't exists, we create it now !
                if(ingredient == null) {
                    int idIngredient = ingredientService.create(components.get(i).getIngredient().getName(),components.get(i).getIngredient().getUnit().name());
                    if(idIngredient == -1) {
                        wrongData = true;
                        break;
                    }else
                        component.setIngredient(ingredientService.findById(idIngredient));
                }else
                    component.setIngredient(ingredient);

                recipe.add(component);

            }else
                wrongData = true;

        }

        if(!wrongData && recipe.isValid()) {
            Recipe newRecipe = recipeRepository.save(recipe);
            if (newRecipe != null)
                id = newRecipe.getId();
        }else
            id = -2;

        return id;

    }

    public boolean deleteById(int idRecipe) {
        int oldSize = recipeRepository.findAll().size();
        recipeRepository.delete(idRecipe);
        int newSize = recipeRepository.findAll().size();
        return newSize < oldSize;
    }
}


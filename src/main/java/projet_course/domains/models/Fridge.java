package projet_course.domains.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Fridge {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="fridge_seq_gen")
    @SequenceGenerator(name="fridge_seq_gen", sequenceName="FRIDGE_SEQ")
    private int id;

    @OneToMany(mappedBy = "fridge", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Component> components;

    public Fridge(){
        this.components = new ArrayList<>();
    }

    public Fridge(int id){
        this.id = id;
        this.components = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void add(Component component){
        components.add(component);
        //Bidirectional relation
        component.setFridge(this);
    }



    public List<Component> getComponents() {
        return components;
    }

}


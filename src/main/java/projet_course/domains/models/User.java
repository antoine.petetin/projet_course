package projet_course.domains.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.springframework.util.StringUtils;
import projet_course.config.security.SHA1;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.*;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="user_seq_gen")
    @SequenceGenerator(name="user_seq_gen", sequenceName="USER_SEQ")
    private int id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Email
    private String mail;

    @JsonIgnore
    @Length(min = 6)
    private String password;

    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "FRIDGE_ID")
    protected Fridge fridge;

    public User(){
    }

    public User(String firstName, String lastName, String mail, String password) {
        this.firstName = StringUtils.capitalize(firstName.trim());
        this.lastName = StringUtils.capitalize(lastName.trim());
        this.mail = mail;
        this.password = password;

    }

    public void cryptPassword(){
        password = SHA1.crypt(password);
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public Fridge getFridge() {
        return this.fridge;
    }

    public void setFridge(Fridge fridge) {
        this.fridge = fridge;
    }

    private boolean isValid(String name){
        return !name.trim().isEmpty() && name.chars().allMatch(Character::isLetter);
    }

    @JsonIgnore
    public boolean isValid(){
        return isValid(firstName) && isValid(lastName) && isValidEmailAddress() && isValidPassword();
    }

    private boolean isValidPassword() {

        return !password.trim().isEmpty() && password.length() >= 6;
    }

    private boolean isValidEmailAddress() {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(mail);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }
}


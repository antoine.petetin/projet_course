package projet_course.domains.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Ingredient {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="ingredient_seq_gen")
    @SequenceGenerator(name="ingredient_seq_gen", sequenceName="INGREDIENT_SEQ")
    private int id;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private Unit unit;

    //To allow cascade remove
    @JsonIgnore
    @OneToMany(mappedBy = "ingredient",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<Component> linkedComponents;

    public Ingredient(){
        linkedComponents = new ArrayList<>();
    }

    public Ingredient(int id, String name, Unit unit){
        this.id = id;
        this.name = StringUtils.capitalize(name.trim());
        this.unit = unit;
        linkedComponents = new ArrayList<>();
    }

    public Ingredient(String name, Unit unit){
        this.name = name;
        this.unit = unit;
        this.linkedComponents = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setName(String name) {
        this.name = StringUtils.capitalize(name.trim());
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public void linkWithComponent(Component component) {
        linkedComponents.add(component);
    }

    public void deleteLinkedComponents() {
        linkedComponents.clear();
    }

    public List<Component> getLinkedComponents() {
        return linkedComponents;
    }
}

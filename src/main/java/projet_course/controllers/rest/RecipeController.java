package projet_course.controllers.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import projet_course.config.exceptions.InvalidParametersException;
import projet_course.config.exceptions.NotCreatedException;
import projet_course.config.exceptions.NotDeletedException;
import projet_course.config.exceptions.ResourceNotFoundException;
import projet_course.domains.models.Component;
import projet_course.domains.models.Recipe;
import projet_course.domains.services.GoldenDataService;
import projet_course.domains.services.RecipeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

@RestController
@RequestMapping(value = "/recipes")
public class RecipeController {
    private RecipeService recipeService;

    static Logger log = Logger.getLogger(RecipeController.class.getName());

    @Autowired()
    public RecipeController(RecipeService recipeService, GoldenDataService goldenDataService){
        this.recipeService = recipeService;
        goldenDataService.initialize();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public int createRecipe(@RequestParam(value = "name") String name, @RequestParam(value = "description") String description, @RequestParam(value = "components")String componentsString) {

        int id=-1;

        //Parse componentsString in List<Component>
        List<Component> components = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
             components = mapper.readValue(componentsString,new TypeReference<List<Component>>(){});
        } catch (Exception e) {
            log.debug(e.getMessage());
        }

        id = recipeService.create(name,description, components);

        if(id == -1)
            throw new NotCreatedException();
        else if(id == -2)
            throw new InvalidParametersException();

        return id;

    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Map<String, Object>> findAll(@RequestParam(name = "iduser") String iduser) {

        int idUser = Integer.parseInt(iduser);
        return recipeService.all(idUser);

    }

    @RequestMapping(value = "/{idrecipe}", method = RequestMethod.GET)
    public Recipe findById(@PathVariable String idrecipe, @RequestParam("iduser") String iduser) {

        int idRecipe = Integer.parseInt(idrecipe);
        int idUser = Integer.parseInt(iduser);

        Recipe recipe = recipeService.findById(idRecipe, idUser);
        if(recipe==null)
            throw new ResourceNotFoundException();

        return recipe;

    }

    @RequestMapping(method= RequestMethod.DELETE, value = "/{idrecipe}")
    public boolean delete(@PathVariable("idrecipe") String idrecipe){
        int idRecipe = Integer.parseInt(idrecipe);

        if (!recipeService.exists(idRecipe)) {
            throw new ResourceNotFoundException();
        }

        boolean deleted = recipeService.deleteById(idRecipe);
        if(!deleted)
            throw new NotDeletedException();

        return deleted;
    }


}

package projet_course.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projet_course.domains.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}


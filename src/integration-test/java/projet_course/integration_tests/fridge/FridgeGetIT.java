package projet_course.integration_tests.fridge;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import projet_course.domains.models.Component;
import projet_course.domains.models.Fridge;
import projet_course.domains.models.Ingredient;
import projet_course.domains.models.Unit;
import projet_course.integration_tests.RestAssuredTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;

public class FridgeGetIT extends RestAssuredTest {

    /* Positive test */

    @Test
    public void should_find_fridge_by_an_existing_user_id() {

        //We are using Jackson to serialize a projet_course.integration_tests.fridge to json object
        Fridge f1 = new Fridge(2);

        Component c1 = new Component(10);
        c1.setIngredient(new Ingredient(9,"Levure chimique", Unit.GRAMME));
        c1.setQuantity(50);
        f1.add(c1);

        Component c2 = new Component(11);
        c2.setIngredient(new Ingredient(1,"Oeuf", Unit.NO_UNIT));
        c2.setQuantity(4);
        f1.add(c2);

        Component c3 = new Component(12);
        c3.setIngredient(new Ingredient(5,"Yahourt nature", Unit.NO_UNIT));
        c3.setQuantity(5);
        f1.add(c3);

        Component c4 = new Component(13);
        c4.setIngredient(new Ingredient(4,"Sucre vanillé", Unit.GRAMME));
        c4.setQuantity(150);
        f1.add(c4);

        Component c5 = new Component(14);
        c5.setIngredient(new Ingredient(6,"Sucre", Unit.GRAMME));
        c5.setQuantity(200);
        f1.add(c5);

        Component c6 = new Component(15);
        c6.setIngredient(new Ingredient(7,"Farine", Unit.GRAMME));
        c6.setQuantity(352);
        f1.add(c6);

        Component c7 = new Component(16);
        c7.setIngredient(new Ingredient(8,"Huile de tournesol", Unit.ML));
        c7.setQuantity(1000);
        f1.add(c7);

        Component c8 = new Component(17);
        c8.setIngredient(new Ingredient(2,"Beurre", Unit.GRAMME));
        c8.setQuantity(250);
        f1.add(c8);

        Component c9 = new Component(18);
        c9.setIngredient(new Ingredient(10,"Pomme", Unit.NO_UNIT));
        c9.setQuantity(4);
        f1.add(c9);

        Component c10 = new Component(19);
        c10.setIngredient(new Ingredient(11,"Cassonade", Unit.GRAMME));
        c10.setQuantity(500);
        f1.add(c10);

        ObjectMapper mapper = new ObjectMapper();
        String json = "";

        try {
            json = mapper.writeValueAsString(f1);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        given()
                .pathParam("iduser",2)
                .when()
                .get("/fridges/{iduser}")
                .then()
                .statusCode(SC_OK)
                .body(equalTo(json));
    }

    /* Negative test */
    @Test
    public void should_not_find_fridge_by_an_non_existing_user_id() {
        given()
                .pathParam("iduser",99)
                .when()
                .get("/fridges/{iduser}")
                .then()
                .statusCode(SC_NOT_FOUND);
    }


}


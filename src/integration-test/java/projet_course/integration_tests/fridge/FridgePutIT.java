package projet_course.integration_tests.fridge;

import org.junit.Test;
import projet_course.integration_tests.RestAssuredTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.junit.Assert.assertTrue;

public class FridgePutIT extends RestAssuredTest {

    /* Positive test*/

    @Test
    public void should_update_component_and_increase_quantity() {
        float quantityToAdd = 3;

        //Get the old quantity
        float oldQuantity = given()
                .pathParam("iduser",1)
                .when()
                .get("/fridges/{iduser}")
                .path("components[0].quantity");

        given()
                .queryParam("idcomponent",1)
                .queryParam("quantity",3)
                .pathParam("iduser",1)
                .when()
                .put("/fridges/{iduser}")
                .then()
                .statusCode(SC_OK);

        float newQuantity = given()
                .pathParam("iduser",1)
                .when()
                .get("/fridges/{iduser}")
                .path("components[0].quantity");

        assertTrue(newQuantity == (oldQuantity + quantityToAdd));
    }

    @Test
    public void should_update_component_and_decrease_quantity() {
        float quantityToRemove = -200;

        //Get the old quantity
        float oldQuantity = given()
                .pathParam("iduser",1)
                .when()
                .get("/fridges/{iduser}")
                .path("components[1].quantity");

        given()
                .queryParam("idcomponent",2)
                .queryParam("quantity",quantityToRemove)
                .pathParam("iduser",1)
                .when()
                .put("/fridges/{iduser}")
                .then()
                .statusCode(SC_OK);

        float newQuantity = given()
                .pathParam("iduser",1)
                .when()
                .get("/fridges/{iduser}")
                .path("components[1].quantity");

        assertTrue(newQuantity == (oldQuantity + quantityToRemove));
    }

    /* Negative tests */

    //Component not found
    @Test
    public void should_not_update_a_unknown_component() {

        given()
                .queryParam("idcomponent",99)
                .queryParam("quantity",15)//We don't care !
                .pathParam("iduser",1)
                .when()
                .put("/fridges/{iduser}")
                .then()
                .statusCode(SC_NOT_FOUND);
    }

    //Unknown user
    @Test
    public void should_not_update_a_unknown_user() {

        given()
                .queryParam("idcomponent",1)
                .queryParam("quantity",15)//We don't care !
                .pathParam("iduser",99)
                .when()
                .put("/fridges/{iduser}")
                .then()
                .statusCode(SC_NOT_FOUND);
    }

    //Want to modify a component which is not in his fridge
    @Test
    public void should_not_update_a_component_in_an_other_fridge() {

        given()
                .queryParam("idcomponent",1)
                .queryParam("quantity",15)//We don't care !
                .pathParam("iduser",2)
                .when()
                .put("/fridges/{iduser}")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    //Want to remove more than we have in fridge
    @Test
    public void should_not_update_and_decrease_quantity_more_than_we_have_in_our_fridge() {

        given()
                .queryParam("idcomponent",3)
                .queryParam("quantity",-800)//We don't care !
                .pathParam("iduser",1)
                .when()
                .put("/fridges/{iduser}")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void should_delete_component_in_fridge_when_final_quantity_is_zero() {

        int oldQuantity = given()
                .pathParam("iduser",1)
                .when()
                .get("/fridges/{iduser}")
                .path("components.size()");

        given()
                .queryParam("idcomponent",9)
                .queryParam("quantity",-500)//Exact the opposite of the left quantity
                .pathParam("iduser",1)
                .when()
                .put("/fridges/{iduser}")
                .then()
                .statusCode(SC_OK);

        int newQuantity = given()
                .pathParam("iduser",1)
                .when()
                .get("/fridges/{iduser}")
                .path("components.size()");

        assertTrue(newQuantity == (oldQuantity - 1));
    }


}


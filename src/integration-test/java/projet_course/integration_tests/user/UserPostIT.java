package projet_course.integration_tests.user;

import org.junit.Test;
import projet_course.integration_tests.RestAssuredTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;

public class UserPostIT extends RestAssuredTest {

    /* Positive test*/

    @Test
    public void should_create_user_with_goods_params() {

        given()
                .formParam("firstname","margot")
                .formParam("lastname","beniche")
                .formParam("mail","margot.beniche@gmail.com")
                .formParam("password","mypassword")
                .post("/users")
                .then()
                .statusCode(SC_CREATED);
    }

    /* Negative tests */

    //See UserTest for unit tests
    @Test
    public void should_not_create_user_with_bads_params() {

        given()
                .formParam("firstname","margotdu69")
                .formParam("lastname","beniche")
                .formParam("mail","margot.beniche@gmail.com")
                .formParam("password","mypassword")
                .post("/users")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void should_not_create_user_with_an_existing_mail() {

        given()
                .formParam("firstname","margotdu69")
                .formParam("lastname","beniche")
                .formParam("mail","a.b@gmail.com")
                .formParam("password","mypassword")
                .post("/users")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }


}


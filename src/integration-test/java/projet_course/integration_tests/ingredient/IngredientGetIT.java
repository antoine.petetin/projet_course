package projet_course.integration_tests.ingredient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import projet_course.domains.models.Ingredient;
import projet_course.domains.models.Unit;
import projet_course.integration_tests.RestAssuredTest;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;

public class IngredientGetIT extends RestAssuredTest {

    @Test
    public void should_find_ingredient_Lait_by_name() {
        List<Ingredient> ingredients = new ArrayList<>();
        Ingredient lait = new Ingredient(12,"Lait", Unit.ML);
        ingredients.add(lait);

        ObjectMapper mapper = new ObjectMapper();

        try {
            String expected = mapper.writeValueAsString(ingredients);
            String actual = mapper.writeValueAsString(
                    given()
                            .pathParam("name","Lait")
                            .when()
                            .get("/ingredients/{name}")
                            .path(""));
            assertJsonEquals(actual, expected);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_find_ingredient_by_name_without_carrying_case() {
        List<Ingredient> ingredients = new ArrayList<>();
        Ingredient lait = new Ingredient(12,"Lait", Unit.ML);
        ingredients.add(lait);

        ObjectMapper mapper = new ObjectMapper();

        try {
            String expected = mapper.writeValueAsString(ingredients);
            String actual = mapper.writeValueAsString(
                    given()
                            .pathParam("name","lait")
                            .when()
                            .get("/ingredients/{name}")
                            .path(""));
            assertJsonEquals(actual, expected);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_find_2_ingredients_by_name_ch() {

        List<Ingredient> ingredients = new ArrayList<>();

        Ingredient chocolatNoir = new Ingredient(3,"Chocolat noir", Unit.GRAMME);
        ingredients.add(chocolatNoir);
        Ingredient levureChimique = new Ingredient(9,"Levure chimique", Unit.GRAMME);
        ingredients.add(levureChimique);

        ObjectMapper mapper = new ObjectMapper();

        try {
            String expected = mapper.writeValueAsString(ingredients);
            String actual = mapper.writeValueAsString(
                    given()
                            .pathParam("name","ch")
                            .when()
                            .get("/ingredients/{name}")
                            .path(""));
            assertJsonEquals(actual, expected);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


}


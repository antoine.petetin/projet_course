package projet_course.integration_tests.ingredient;

import org.junit.Test;
import projet_course.integration_tests.RestAssuredTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;

public class IngredientDeleteIT extends RestAssuredTest {

    /* Positive test*/

    /*
    @Test
    public void should_delete_cascade_an_existing_ingredient_in_fridge_1_and_in_recipe_2_and_3() {
        //Retrieve the old size of the first fridge component list
        int oldSize = given()
                .pathParam("iduser",1)
                .when()
                .get("/fridges/{iduser}")
                .path("components.size()");


        //We delete an ingredient linked to a fridge
        given()
                .pathParam("id",12)
                .delete("/ingredients/{id}")
                .then()
                .statusCode(SC_OK);

        //Test if the fridge has changed
        int newSize = given()
                .pathParam("iduser",1)
                .when()
                .get("/fridges/{iduser}")
                .path("components.size()");

        assertTrue(newSize == (oldSize -1));

    }*/

    /* Negative tests */

    @Test
    public void should_not_delete_ingredient_which_does_not_already_exist() {
        given()
                .pathParam("id",99)
                .delete("/ingredients/{id}")
                .then()
                .statusCode(SC_NOT_FOUND);
    }
}


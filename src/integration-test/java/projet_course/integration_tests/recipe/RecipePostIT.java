package projet_course.integration_tests.recipe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import projet_course.domains.models.Component;
import projet_course.domains.models.Ingredient;
import projet_course.domains.models.Unit;
import projet_course.integration_tests.RestAssuredTest;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;

public class RecipePostIT extends RestAssuredTest {

    /* Positive test*/

    //In this recipe, we have 1 ingredient that already exist and one to create lazily
    @Test
    public void should_create_recipe_with_valid_name_description_and_components() {
        String description = "1 - Beurrer une petite poêle. Faire chauffer sur feu moyen.\n" +
                "2 - Lorsque le beurre est fondu, casser délicatement les oeufs dans la poêle sans crever le jaune. Saler le blanc mais pas le jaune qui se tacherait de petits points blancs.\n" +
                "3 - Pour finir, laisser cuire 2 ou 3 minutes en étalant légèrement le blanc avec une spatule. Poivrer. Faire glisser les oeufs sur les assiettes à l'aide de la spatule. ";

        List<Component> componentList = new ArrayList<>();
        //This ingredient already exists
        Component c1 = new Component();
        c1.setIngredient(new Ingredient("Oeuf", Unit.NO_UNIT));
        c1.setQuantity(1);

        componentList.add(c1);

        //This ingredient have to be created lazily
        Component c2 = new Component();
        c2.setIngredient(new Ingredient("Poivre",Unit.GRAMME));
        c2.setQuantity(200);

        componentList.add(c2);

        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(componentList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        given()
                .formParam("name","Oeuf au plat")
                .formParam("description", description)
                .formParam("components", json)
                .post("/recipes")
                .then()
                .statusCode(SC_CREATED);
    }

    /* Negative tests */
    //invalid name (empty or not)
    @Test
    public void should_not_create_recipe_when_name_is_invalid() {
        String description = "1 - Beurrer une petite poêle. Faire chauffer sur feu moyen.\n" +
                "2 - Lorsque le beurre est fondu, casser délicatement les oeufs dans la poêle sans crever le jaune. Saler le blanc mais pas le jaune qui se tacherait de petits points blancs.\n" +
                "3 - Pour finir, laisser cuire 2 ou 3 minutes en étalant légèrement le blanc avec une spatule. Poivrer. Faire glisser les oeufs sur les assiettes à l'aide de la spatule. ";

        List<Component> componentList = new ArrayList<>();
        //This ingredient already exists
        Component c1 = new Component();
        c1.setIngredient(new Ingredient("Oeuf", Unit.NO_UNIT));
        c1.setQuantity(1);

        componentList.add(c1);

        //This ingredient have to be created lazily
        Component c2 = new Component();
        c2.setIngredient(new Ingredient("Poivre",Unit.GRAMME));
        c2.setQuantity(200);

        componentList.add(c2);

        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(componentList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        given()
                .formParam("name","nom de recette 69")
                .formParam("description", description)
                .formParam("components", json)
                .post("/recipes")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    //Description is empty
    @Test
    public void should_not_create_recipe_when_description_is_empty() {
        String description = " ";

        List<Component> componentList = new ArrayList<>();
        //This ingredient already exists
        Component c1 = new Component();
        c1.setIngredient(new Ingredient("Oeuf", Unit.NO_UNIT));
        c1.setQuantity(1);

        componentList.add(c1);

        //This ingredient have to be created lazily
        Component c2 = new Component();
        c2.setIngredient(new Ingredient("Poivre",Unit.GRAMME));
        c2.setQuantity(200);

        componentList.add(c2);

        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(componentList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        given()
                .formParam("name","Oeuf au plat")
                .formParam("description", description)
                .formParam("components", json)
                .post("/recipes")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }

    //Empty component

    @Test
    public void should_not_create_recipe_when_name_is_empty() {
        String description = "1 - Beurrer une petite poêle. Faire chauffer sur feu moyen.\n" +
                "2 - Lorsque le beurre est fondu, casser délicatement les oeufs dans la poêle sans crever le jaune. Saler le blanc mais pas le jaune qui se tacherait de petits points blancs.\n" +
                "3 - Pour finir, laisser cuire 2 ou 3 minutes en étalant légèrement le blanc avec une spatule. Poivrer. Faire glisser les oeufs sur les assiettes à l'aide de la spatule. ";

        given()
                .formParam("name","Oeuf au plat")
                .formParam("description", description)
                .formParam("components", "[]")
                .post("/recipes")
                .then()
                .statusCode(SC_BAD_REQUEST);
    }
}


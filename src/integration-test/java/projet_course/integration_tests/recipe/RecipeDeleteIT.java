package projet_course.integration_tests.recipe;

import org.junit.Test;
import projet_course.integration_tests.RestAssuredTest;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.hasSize;

public class RecipeDeleteIT extends RestAssuredTest {

    /* Positive test*/

    @Test
    public void should_delete_an_existing_recipe_with_theirs_components() {

        given()
                .pathParam("id",5)
                .delete("/recipes/{id}")
                .then()
                .statusCode(SC_OK);

    }

    /* Negative tests */

    @Test
    public void should_not_delete_an_unknown_recipe() {
        given()
                .pathParam("id",99)
                .delete("/recipes/{id}")
                .then()
                .statusCode(SC_NOT_FOUND);
    }
}


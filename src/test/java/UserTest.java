import org.junit.Assert;
import org.junit.Test;
import projet_course.domains.models.User;

public class UserTest {

    /* Positive test */
    @Test
    public void should_is_valid_name_return_true_on_good_name() {
        User user = new User("antoine","petetin","antoine@gmail.com", "123456");
        Assert.assertTrue(user.isValid());
    }

    /* Negative tests */

    @Test
    public void should_is_valid_name_return_false_on_bad_firname() {
        User user = new User("antoineDu38","petetin","antoine@gmail.com", "123456");
        Assert.assertFalse(user.isValid());
    }

    @Test
    public void should_is_valid_name_return_false_on_bad_name() {
        User user = new User("antoine","petetinDu38","antoine@gmail.com", "123456");
        Assert.assertFalse(user.isValid());
    }

    @Test
    public void should_is_valid_name_return_false_on_bad_email() {
        User user = new User("antoine","petetin","antoinepasdarobasegmail.com", "123456");
        Assert.assertFalse(user.isValid());
    }

    @Test
    public void should_is_valid_name_return_false_on_too_short_password() {
        User user = new User("antoine","petetin","antoine@gmail.com", "123");
        Assert.assertFalse(user.isValid());
    }



}
